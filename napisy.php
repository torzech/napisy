#!/usr/bin/env php
<?php

require 'vendor/autoload.php';
require 'libs/Engine.php';

$availableLangs = ['pl', 'en'];
$ignoredExtensions = ['txt', 'srt', 'vsmeta'];

$Output = new Symfony\Component\Console\Output\ConsoleOutput();

if ($argc == 1) {
    $Output->writeln('<error>Podaj nazwę pliku z filmem!</error>');
    die();
}

$files = $argv;
array_shift($files);

foreach ($files as $file) {
    if (!file_exists($file)) {
        $Output->writeln(sprintf("<error>Plik '%s' nie istnieje!</error>", $file));
        continue;
    }

    $fileParts = explode('.', $file);
    $extension = array_pop($fileParts);
    if (in_array($extension, $ignoredExtensions)) {
        continue;
    }

    $filePath = sprintf('%s/%s', getcwd(), $file);

    $Engine = new Engine($filePath);

    $Output->write(sprintf('<info>Pobieram napisy dla pliku:</info> <comment>%s</comment> ', $file));

    foreach ($availableLangs as $lang) {
        $Output->write(sprintf('%s', $lang));

        if ($Engine->downloadSubtitles($lang)) {
            $Output->write('<info>✔</info>');
        }

        $Output->write(' ');
    }

    $Output->writeln('');
}
