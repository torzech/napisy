<?php

define('MPLAYER_PATH', "/usr/local/bin/mplayer");
define('ICONV_PATH', "/usr/bin/iconv");

class Engine
{
    private $filePath = null;
    private $baseDir = null;
    private $baseName = null;
    private $srtFilePath = null;
    private $dumpSrtPath = null;
    private $downloadedFilePaths = [];

    public function __construct($filePath)
    {
        $this->filePath = $filePath;
        $this->baseDir = dirname($filePath);

        $baseName = basename($filePath);
        $baseName = explode('.', $baseName);
        array_pop($baseName);
        $this->baseName = implode('.', $baseName);

        $this->srtFilePath = sprintf('%s.%%s.srt', $this->baseName);

        $this->dumpSrtPath = sprintf('%s/dumpsub.srt', $this->baseDir);

        foreach (['txt', 'srt'] as $extension) {
            $this->downloadedFilePaths[] = sprintf('%s.%s', $this->baseName, $extension);
        }
    }

    /**
     * Download subtitles
     *
     * @param string $lang
     * @return boolean
     */
    public function downloadSubtitles($lang)
    {
        $this->qnapiDownload($lang);
        $downloadedFilePath = $this->getDownloadedFilePath();

        if (!$downloadedFilePath) {
            $this->periscopeDownload($lang);
            $downloadedFilePath = $this->getDownloadedFilePath();
        }

        if ($downloadedFilePath) {
            $encoding = $this->detectEncoding(file_get_contents($downloadedFilePath));

            if ($encoding !== 'UTF-8') {
                $this->convertFileToUtf8($encoding, $downloadedFilePath);
            }

            $this->convertToSrt($downloadedFilePath, $lang);

            return true;
        }

        return false;
    }

    /**
     * QNapi
     *
     * @param string $lang
     */
    private function qnapiDownload($lang)
    {
        $cmd = sprintf('%s -c -q -d -l %s %s > /dev/null 2>&1', $this->getQnapiPath(), $lang, escapeshellarg($this->filePath));
        system($cmd);
    }

    /**
     * Periscope
     *
     * @param string $lang
     */
    private function periscopeDownload($lang)
    {
        $cmd = sprintf('%s --quiet -f -l %s %s > /dev/null 2>&1', $this->getPeriscopePath(), $lang, escapeshellarg($this->filePath));
        system($cmd);
    }

    /**
     * Detect encoding
     *
     * @param string $string
     * @return string
     */
    private function detectEncoding($string)
    {
        $win2utf = [
            "\xb9" => "\xc4\x85", "\xa5" => "\xc4\x84", "\xe6" => "\xc4\x87", "\xc6" => "\xc4\x86",
            "\xea" => "\xc4\x99", "\xca" => "\xc4\x98", "\xb3" => "\xc5\x82", "\xa3" => "\xc5\x81",
            "\xf3" => "\xc3\xb3", "\xd3" => "\xc3\x93", "\x9c" => "\xc5\x9b", "\x8c" => "\xc5\x9a",
            "\x9f" => "\xc5\xba", "\xaf" => "\xc5\xbb", "\xbf" => "\xc5\xbc", "\xac" => "\xc5\xb9",
            "\xf1" => "\xc5\x84", "\xd1" => "\xc5\x83", "\x8f" => "\xc5\xb9"
        ];

        $countWin = 0;
        $found = 0;

        foreach ($win2utf as $win => $utf) {
            $pos = strpos($string, $win);
            if ($pos !== false) {
                $found++;
            }

            if ($pos && substr(iconv('WINDOWS-1250', 'UTF-8', $string), $pos + $countWin, 2) == $utf) {
                $countWin++;
            }
        }

        if ($countWin > 0 && $found == $countWin) {
            return 'WINDOWS-1250';
        }

        if ($found) {
            return 'ISO-8859-2';
        }

        return 'UTF-8';
    }

    /**
     * Convert file to UTF-8
     *
     * @param string $sourceEncoding
     * @param string $filePath
     */
    private function convertFileToUtf8($sourceEncoding, $filePath) {
        $filePathTmp = sprintf('%s~', $filePath);

//        $cmd = sprintf('%s -f %s -t utf-8 %s > %s', ICONV_PATH, $sourceEncoding, escapeshellarg($filePath), escapeshellarg($filePathTmp));
        $cmd = sprintf('%s -f %s -t utf-8 %s > %s', ICONV_PATH, 'CP1250', escapeshellarg($filePath), escapeshellarg($filePathTmp));
        system($cmd);

        $cmd = sprintf('mv %s %s', escapeshellarg($filePathTmp), escapeshellarg($filePath));
        system($cmd);
    }

    /**
     * Convert to SRT
     *
     * @param string $subtitlePath
     * @param string $lang
     */
    private function convertToSrt($subtitlePath, $lang)
    {
        $cmd = sprintf(
            "echo 'q' | mplayer -sub %s -nosound -vo null -dumpsrtsub %s > /dev/null 2>&1",
            escapeshellarg($subtitlePath),
            escapeshellarg($this->filePath)
        );
        system($cmd);

        $subtitlePathLang = sprintf($this->srtFilePath, $lang);
        rename($this->dumpSrtPath, $subtitlePathLang);
        unlink($subtitlePath);
    }

    /**
     * Get downloaded file path
     *
     * @return string|null
     */
    private function getDownloadedFilePath()
    {
        $downloadedFilePath = null;
        foreach($this->downloadedFilePaths as $path) {
            if (file_exists($path)) {
                $downloadedFilePath = $path;
            }
        }

        return $downloadedFilePath;
    }

    /**
     * Get QNapi path
     *
     * @return string
     */
    private function getQnapiPath()
    {
        return realpath(__DIR__ . '/QNapi.app/Contents/MacOS/QNapi');
    }

    /**
     * Get Periscope path
     *
     * @return string
     */
    private function getPeriscopePath()
    {
        return realpath(__DIR__ . '/periscope/periscope.py');
    }
}
